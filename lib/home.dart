import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'GridView/gridMasN.dart';

class MyAppStart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridViewScreen()
    );  
  }
}

Widget toastFunstion(BuildContext context, String msg) {
  return Flushbar(
    margin: EdgeInsets.only(
        left: 15, right: 15, bottom: 50),
    borderRadius: 8,
    flushbarStyle: FlushbarStyle.FLOATING,
    // message: msg,
    messageText: Text(
      msg,
      style: TextStyle(fontSize: 16.0, color: Colors.white)
      ),
    duration: Duration(seconds: 5),
  )..show(context);
}