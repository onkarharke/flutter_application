import 'package:flutter/material.dart';
import 'package:flutter_application_1/home.dart';
import 'package:flutter_application_1/GridView/createGridView.dart';

class GridViewScreen extends StatefulWidget {
  @override
  _GridViewScreenState createState() => _GridViewScreenState();
}

class _GridViewScreenState extends State<GridViewScreen> {
  TextEditingController m = new TextEditingController();
  TextEditingController n = new TextEditingController();
  final myRowController = TextEditingController();
  final myColController = TextEditingController();
  int row = 0;
  int col = 0;
  String _textString;
  @override
  void dispose() {
    myRowController.dispose();
    myColController.dispose();
    super.dispose();
  }
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text("2D View")),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(15),
            child: Column(  
              children: <Widget>[  
                Padding(  
                  padding: EdgeInsets.all(15),  
                  child: TextField(
                    decoration: InputDecoration(  
                      border: OutlineInputBorder(),  
                      labelText: 'M (Row)',  
                      hintText: 'Enter Number of Row',  
                    ),
                    controller: myRowController,
                  ), 
                ),  
                Padding(  
                  padding: EdgeInsets.all(15),  
                  child: 
                  TextField(
                    decoration: InputDecoration(  
                      border: OutlineInputBorder(),  
                      labelText: 'N (Column)',  
                      hintText: 'Enter Number of Column',  
                    ),
                    controller: myColController,
                  ),

                ),  
                RaisedButton( 
                  textColor: Colors.white,  
                  color: Colors.blue,  
                  child: Text('Create Grid View'),
                  disabledColor: Colors.grey,
                  onPressed: (){
                    
                    print(myRowController.text);
                    print(myColController.text);
                    if (myRowController.text == '' || myColController.text == '') {
                   print("in if");
                   toastFunstion(context, "Please enter both values M & N!");
                    
                      
                    }else{
                      print("in else");
                        Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => createGridViewMN(myRowController.text,myColController.text)),
                  );
                      
                    }
                  
                  },  
                ),
                Divider(),
                Container(
                  child: Text("Note* : Tap on grid to assign character"),
                )
              ],  
            ) ,
          ),
          
        ],
      )
    );
  }
}

