import 'package:flutter/material.dart';
import 'package:flutter_application_1/home.dart';


int m;
int n;
int items;
// List<> choices = <String>[];
var choices= [];
var present = [];

class createGridViewMN extends StatefulWidget 
{
  createGridViewMN(String row, String col){
    m = int.parse(row);
    n = int.parse(col);
    print(m);
    print(n);
    items = m*n;
    print(items);
    choices = [];
    for (var i = 0; i < items; i++) {
      choices.add('--');
    }
  }
    

  @override
  _createGridViewMNState createState() => _createGridViewMNState();
}


class _createGridViewMNState extends State<createGridViewMN> {
  final searchwordController = TextEditingController();
  final myCharController = TextEditingController();
  final myTextSearchController = TextEditingController();
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text("2D View")),
      body: Container(
        padding: EdgeInsets.all(12.0),  
            child: GridView.builder(
              itemCount: items,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: n),
                  itemBuilder: (context, index) => 
                  Card(
                    elevation:  2,
                    // color: present.length >= n ? Colors.blueAccent : Colors.amber,
                    child: GridTile(
                    child: InkWell(
                        child:  Center( child:
                        choices.length < m ? Text("--") : Text(choices[index], style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),)
                        ,),
                        onTap: () {
                          print(index);
                          takePhoto(context,index);
                        }
                    ),
                  ),
                  )
                )),
                floatingActionButton: FloatingActionButton(
        onPressed: () {
          searchDialog(context);
        },
        child: const Icon(Icons.search),
        backgroundColor: Colors.green,
      ),
    ); 
  }

  takePhoto(BuildContext context, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext ctxt) {
          return AlertDialog(
            title: Text("Add Character"),
            content: Container(
              height: 150,
              width: 80,
              child: Column(
                children: <Widget>[
                  //Divider(),
                  TextField(
                    decoration: InputDecoration(  
                      border: OutlineInputBorder(),  
                      labelText: 'Character',  
                      hintText: 'Enter Character',  
                    ),
                    controller: myCharController,
                  ), 
                  Divider(),
                   RaisedButton( 
                  textColor: Colors.white,  
                  color: Colors.blue,  
                  child: Text('Save'),
                  disabledColor: Colors.grey,
                  onPressed: (){
                    if (myCharController.text == "") {
                      toastFunstion(context, "Please enter character!");
                    }else{
                      saveMyChar(myCharController.text,index);
                      print(myCharController.text);
                      Navigator.pop(context);
                    }
                  },  
                ) 
                ],
              ),
            ),
          );
        });
  }

  searchDialog(BuildContext context){
    return  
    showDialog(
        context: context,
        //barrierDismissible: false,
        builder: (BuildContext ctxt) {
          return AlertDialog(
            title: Text("Search Text"),
            content: Container(
              height: 150,
              width: 80,
              child: Column(
                children: <Widget>[
                  //Divider(),
                  TextField(
                    decoration: InputDecoration(  
                      border: OutlineInputBorder(),  
                      labelText: 'Search Text',  
                      hintText: 'Enter Text',  
                    ),
                    controller: myTextSearchController,
                  ), 
                  Divider(),
                   RaisedButton( 
                  textColor: Colors.white,  
                  color: Colors.blue,  
                  child: Text('Search'),
                  disabledColor: Colors.grey,
                  onPressed: (){
                    print(myTextSearchController.text);
                    searchMyCharacter(myTextSearchController.text);
                    Navigator.pop(context);
                    // print(present);
                    // if (present) {
                    //   print("if present");
                    //   Navigator.pop(context);
                    //   
                    // }
                  },  
                ) 
                ],
              ),
            ),
          );
        });
  }

  void saveMyChar(text,index){
    print("in savemychar");
    //  Choice a = choices[index];
    print(text);
    print(index);
   
    setState(() {
           choices.removeAt(index);
           choices.insert(index, text);
        }); 
        print(choices);
  }

  searchMyCharacter(searchText){
    print(searchText);
    //Search text for row
   bool isPresent = false;
    var textSeach = '';
    
    
    for (var i = 0; i < m; i++) {
      print("value of i -");
      print(i);
      textSeach = '';
      print("textSeach");
      print(textSeach);
      for (var j = 0; j < choices.length; j++) {
        
        if (choices[j]!= "--") {
          textSeach += choices[j];
        }
        
        
        
        print("textSeach");
        print(textSeach);
        if (textSeach == searchText) {
          print("String Found");
          print(textSeach + " : " +searchText);
          isPresent = true;
          toastFunstion(context, "String " + textSeach + " is present...");
          textSeach = '';
          break;
        }else{
          print("in else");
          textSeach = '';
          continue;
        }
        
      }
      
    }
    //Check for col
    if (!isPresent) {
      print("in if 217");
      searchColumnTest(searchText);
    }
    
  }

  searchColumnTest(user_input_string){
    print("in searchColumn  " +user_input_string+ "  :  " );
    String textSeach;
    for (var i = 0; i < m; i++) {
      print("value of i -");
      print(i);
      textSeach = '';
      print("textSeach");
      print(textSeach);
      for (var j = 0; j < m; j++) {
        
        if (choices[j]!= "--" && j == 0) {
          textSeach += choices[j];
        }
        
        
        
        print("textSeach");
        print(textSeach);
        if (textSeach == user_input_string) {
          print("String Found");
          print(textSeach + " : " +user_input_string);
          
          toastFunstion(context, "String " + textSeach + " is present at Column...");
          break;
        }
        
      }
      
    }
  }
}

